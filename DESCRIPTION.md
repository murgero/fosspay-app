### Overview

This app packages Fosspay master branch <upstream>https://git.sr.ht/~sircmpwn/fosspay</upstream>

### About

Donation collection for FOSS groups and individuals.

* Supports one-time and monthly donations
* Process cards with Stripe
* Flexible and customizable

It works for individuals (like me) and it works for organizations. Expect to
spend about an hour or two setting up everything and then you're good to go.

### Support

For support, visit [#cmpwn on
irc.freenode.net](http://webchat.freenode.net/?channels=cmpwn&uio=d4)
or file a GitHub issue.