FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code/fosspay /app/data
WORKDIR /app/code


## Install python and requirements
RUN apt-get update && apt-get install -y \
    binutils-doc autoconf flex bison libjpeg-dev \
    libfreetype6-dev libzmq3-dev automake libtool tmux postgresql-server-dev-10 \
    python3 python3-pip python-dev python3-dev python-pip virtualenvwrapper \
    && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

## Get FOSSPAY source
RUN curl -L https://git.sr.ht/~sircmpwn/fosspay/archive/master.tar.gz | tar -xz -C /app/code/fosspay --strip-components 1

## FOSSPAY build - need to compile static assets and copy config
ADD build.sh /app/code/
RUN /bin/bash /app/code/build.sh
COPY config.ini /app/data/config.ini
RUN ln -s /app/data/config.ini /app/code/fosspay/config.ini

## Push start script
COPY start.sh /app/code/start.sh
CMD [ "/app/code/start.sh" ]
