#!/bin/bash

set -eu -o pipefail

echo "========= Build ========="
echo "==> setup virtualenv"
cd /app/code/fosspay
echo "==> install deps"
python3 -m pip install --upgrade pip
python3 -m pip install -U testresources
python3 -m pip install -U setuptools
python3 -m pip install -r requirements.txt
make


