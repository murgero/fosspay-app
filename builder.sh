#!/bin/bash

## Build app
docker build -t mitchellurgero/org.urgero.fosspay:latest . --no-cache 
docker push mitchellurgero/org.urgero.fosspay:latest

## Install app
cloudron install --image=mitchellurgero/org.urgero.fosspay:latest
