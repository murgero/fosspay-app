#!/bin/bash
echo "=> Make cloudron own /run"
chown -R cloudron:cloudron /run
chown -R cloudron:cloudron /app/data

if [[ ! -f /app/data/sec ]]; then
    ## Generate a secret key for use in the ini
    openssl rand -base64 16 > /app/data/sec
fi

echo "=> Setup config"
sed -i.bak "s+^\(connection-string=\).*+\1${CLOUDRON_POSTGRESQL_URL}+" /app/data/config.ini
sed -i.bak "s+^\(domain=\).*+\1${CLOUDRON_APP_DOMAIN}+" /app/data/config.ini
sed -i.bak "s+^\(smtp-host=\).*+\1${CLOUDRON_MAIL_SMTP_SERVER}+" /app/data/config.ini
sed -i.bak "s+^\(smtp-port=\).*+\1${CLOUDRON_MAIL_SMTP_PORT}+" /app/data/config.ini
sed -i.bak "s+^\(smtp-user=\).*+\1${CLOUDRON_MAIL_SMTP_USERNAME}+" /app/data/config.ini
sed -i.bak "s+^\(smtp-password=\).*+\1${CLOUDRON_MAIL_SMTP_PASSWORD}+" /app/data/config.ini
sed -i.bak "s+^\(smtp-from=\).*+\1${CLOUDRON_MAIL_FROM}+" /app/data/config.ini
sed -i.bak "s+^\(secret-key=\).*+\1$(cat /app/data/sec)+" /app/data/config.ini

echo "=> Start fosspay"
cd /app/code/fosspay/
##exec /usr/local/bin/gosu cloudron:cloudron python3 app.py ## <-- Enable for debug mode
exec /usr/local/bin/gosu cloudron:cloudron /usr/local/bin/gunicorn app:app -b 0.0.0.0:5000