#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test SSO', function () {
    this.timeout(0);

    var LOCATION = 'test';
    var TEST_TIMEOUT = 100000;
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var PROJECT_NAME = 'testproject';
    var PROJECT_DESCRIPTION = 'testdescription';
    var USER_STORY_SUBJECT = 'someteststory';

    var browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function login(callback) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn).then(function () {
            return browser.executeScript('localStorage.clear();');
        }).then(function () {
            browser.executeScript('sessionStorage.clear();');
        }).then(function () {
            browser.get('https://' + app.fqdn + '/login?next=%252Fprofile');
        }).then(function () {
            return waitForElement(By.name('username'));
        }).then(function () {
            return browser.findElement(By.name('username')).sendKeys(process.env.USERNAME);
        }).then(function () {
            return browser.findElement(By.name('password')).sendKeys(process.env.PASSWORD);
        }).then(function () {
            return browser.findElement(By.className('login-form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//h4[text()="Your profile"]'));
        }).then(function () {
            callback();
        });
    }

    function signUp(callback) {
        browser.manage().deleteAllCookies();

        browser.get(`https://${app.fqdn}`).then(function () {
            return browser.executeScript('localStorage.clear();');
        }).then(function () {
            return browser.executeScript('sessionStorage.clear();');
        }).then(function () {
            return browser.get(`https://${app.fqdn}/register`);
        }).then(function () {
            return waitForElement(By.name('username'));
        }).then(function () {
            return browser.findElement(By.name('username')).sendKeys(process.env.USERNAME);
        }).then(function () {
            return browser.findElement(By.name('full_name')).sendKeys(process.env.USERNAME + ' Name');
        }).then(function () {
            return browser.findElement(By.name('email')).sendKeys('test@cloudron.io');
        }).then(function () {
            return browser.findElement(By.name('password')).sendKeys(process.env.PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//button[@title="Sign up"]')).click();
        }).then(function () {
            return waitForElement(By.xpath(`//*[contains(text(), "${process.env.USERNAME} Name")]`));
        }).then(function () {
            callback();
        });
    }

    function adminLogin(callback) {
        browser.get('https://' + app.fqdn + '/admin/login/').then(function () {
            return browser.findElement(By.name('username')).sendKeys('admin');
        }).then(function () {
            return browser.findElement(By.name('password')).sendKeys('123123');
        }).then(function () {
            return browser.findElement(By.id('login-form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//h1[text()="Site administration"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[text()="Log out"]')).click();
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            callback();
        });
    }

    function userStoryExists(callback) {
        browser.get('https://' + app.fqdn + '/project/' + process.env.USERNAME + '-' + PROJECT_NAME + '/us/1').then(function () {
            return waitForElement(By.xpath('//span[text()="' + USER_STORY_SUBJECT + '"]'));
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    function createProject(done) {
        browser.get('https://' + app.fqdn + '/project/new/scrum').then(function () {
            return waitForElement(By.name('project-name'));
        }).then(function () {
            return browser.findElement(By.name('project-name')).sendKeys(PROJECT_NAME);
        }).then(function () {
            return browser.sleep(1000);    // it needs some time to change focus!!
        }).then(function () {
            return browser.findElement(By.xpath('//textarea[@placeholder="Project Description"]')).sendKeys(PROJECT_DESCRIPTION);
        }).then(function () {
            return browser.sleep(1000);
        }).then(function () {
            return browser.findElement(By.xpath('//button[text()="Create Project"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//span[text()[contains(., "' + PROJECT_NAME + '")]]')), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function createUserStory(done) {
        browser.get('https://' + app.fqdn + '/project/' + process.env.USERNAME + '-' + PROJECT_NAME + '/backlog').then(function () {
            return waitForElement(By.xpath('//a[@tg-check-permission="add_us"]'));
        }).then(function () {
            // clicks don't work with taiga
            return browser.findElement(By.xpath('//a[@tg-check-permission="add_us"]')).sendKeys(Key.ENTER);
        }).then(function () {
            return waitForElement(By.name('subject'));
        }).then(function () {
            return browser.findElement(By.name('subject')).sendKeys(USER_STORY_SUBJECT);
        }).then(function () {
            return waitForElement(By.xpath('/html/body/div[2]/div/div/div[1]/form/div[2]/button'));
        }).then(function () {
            return browser.findElement(By.xpath('/html/body/div[2]/div/div/div[1]/form/div[2]/button')).sendKeys(Key.ENTER);
        }).then(function () {
            return waitForElement(By.xpath('//span[text()[contains(., "' + USER_STORY_SUBJECT + '")]]'));
        }).then(function () {
            done();
        });
    }

    function deleteProject(done) {
        browser.get('https://' + app.fqdn + '/project/' + process.env.USERNAME + '-' + PROJECT_NAME + '/admin/project-profile/details').then(function () {
        return waitForElement(By.className('delete-project'));
        }).then(function () {
            return browser.findElement(By.className('delete-project')).click();
        }).then(function () {
            return waitForElement(By.xpath('//a[@title="Yes, I\'m really sure"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[@title="Yes, I\'m really sure"]')).click();
        }).then(function () {
            // give some time to redirect
            setTimeout(done, 5000);
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app with SSO', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can login', login);
    it('can create project', createProject);
    it('can create user story', createUserStory);
    it('user story exists', userStoryExists);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('user story is still present', userStoryExists);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('user story is still present', userStoryExists);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);

    it('can admin login', adminLogin);
    // origin change requires new login
    it('can login', login);
    it('user story is still present', userStoryExists);

    it('can delete project', deleteProject);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
            done();
        });
    });

    it('install app without SSO', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can sign up', signUp);
    it('can create project', createProject);
    it('can create user story', createUserStory);
    it('user story exists', userStoryExists);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('user story is still present', userStoryExists);
    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id io.taiga.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can login', login);
    it('can create project', createProject);
    it('can create user story', createUserStory);
    it('user story exists', userStoryExists);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can admin login', adminLogin);
    it('user story exists', userStoryExists);
    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
            done();
        });
    });
});
